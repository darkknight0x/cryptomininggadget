

var settings = {};
// var i = 0;
var clrloop;
var fullurl = "";
var appHeight = 300; //Default height
var delayTime =  60000; //default 1 min
$.ajaxSetup({
        cache: false
    });

function init()
{
  System.Gadget.settingsUI = "settings.html";
   jQuery.support.cors = true;
    var oBackground = document.getElementById("imgBackground");
    oBackground.src = "url(images/bckgrd600h.png)";
    
    readSettings();
    delayTime = settings.refresh * 60000;
    updateLoop();

    clrloop = setInterval(updateLoop, delayTime);

}


function updateLoop()
{

  var workers;
  var workcnt = 0;
  var dwork;
   readSettings();

    if(settings.apikey != "none"){
        refresher();
          
    }//End setting.apikey if
  
    $("#upcnt").text(getTime());// Make Sure updating
   // i++;

}

function getTime() {
    var dTime = new Date();
    var hours = dTime.getHours();
    var minute = dTime.getMinutes();

    if(minute < 10){
      var newmin = "0"+minute;
      minute = newmin;
    }
    var period = "AM";
    if (hours > 12) {
        period = "PM";
    }
   else {
       period = "AM";
   }
   hours = ((hours > 12) ? hours - 12 : hours);
   return hours + ":" + minute + " " + period;
}

function refresher(){
  fullurl = settings.apikey;
    $.getJSON(fullurl, function(jsonp) {

      workers = jsonp.workers;
        $("#username").text(jsonp.username );
        $("#cnfrewd").text(jsonp.confirmed_rewards)
        $("#rndest").text(jsonp.round_estimate +'\n\n');
        $("#tothash").text(jsonp.total_hashrate +' kH/s');
        $("#rndshare").text(jsonp.round_shares +'\n\n');
        $("#payhist").text(jsonp.payout_history +'\n\n');
        
        //Go through workers
        var key, workcnt = 0;
        dwork = 0;
        for(key in workers){
          if(workers[key].alive == "1"){// show active workers
              if($("#awrkr"+workcnt).length){//If already exist update text
                 $("#"+key+"hr").text(workers[key].hashrate+'kH/s');
                 $("#"+key).removeClass("deadworker");
              }else{//If Doesn't Exist Need to create
                $("#worknum").append('<div id="awrkr'+workcnt+'"></div>');
                $("#awrkr"+workcnt).append('<div id="'+key+'">'+key+" : "+workers[key].hashrate+' kH/s</div>');
              }//ENd else-if
          }///end  alive if
          else if(workers[key].alive == "0"){//Dead workers
              dwork++;
              if($("#awrkr"+workcnt).length){//If already exist update text
                 $("#"+key+"hr").text(workers[key].hashrate+'kH/s');
                 $("#"+key).addClass("deadworker");
              }else{//If Doesn't Exist Need to create
                $("#worknum").append('<div id="awrkr'+workcnt+'"></div>');
                $("#awrkr"+workcnt).append('<div class ="deadworker" id="'+key+'">'+key+" : "+workers[key].hashrate+' kH/s</div>');
              }//ENd else-if
          }///end  alive if 
          if(workcnt >= 2){//Grow body to fit workers
            var diff = workcnt - 1;
            var $test = $("#mainbody");
            $test[0].style.height = appHeight+(15*diff)+'px';
          }
           workcnt++;

        }//End for
    if(settings.deadworker == "false"){
      $(".deadworker").hide();
    }
    else{
      $(".deadworker").show();
     
        }
    });//END getJSON callback
    
}

//Valid json Object
    // {
    //   "username":"myUname",
    //   "confirmed_rewards":"0.37312026",
    //   "round_estimate":"0.0144739",
    //   "total_hashrate":"272",
    //   "payout_history":"1.00397281",
    //   "round_shares":"250",
    //   "workers":
    //   {
    //     "myUname.2":
    //     {
    //       "alive":"1",
    //       "hashrate":"144"
    //     },
    //     "myUname.1":
    //     {
    //       "alive":"1",
    //       "hashrate":"128"
    //     }
    //   } //End workers
    // } //End json Object
